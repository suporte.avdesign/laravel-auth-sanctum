<p align="center"><img src="https://painel.avdesign.com.br/img/logo/login-title.png"></p>

# Laravel Sanctum Setup

### Dependencias:
```` 
$ composer require laravel/sanctum
$ composer require fruitcake/laravel-cors
````


Tutorial: **[SPA Authentication Laravel Sanctum](https://laravel.com/docs/7.x/sanctum)**.

Tutorial: **[SPA Authentication CORS Middleware](https://github.com/fruitcake/laravel-cors)**.


### Instale as dependências do composer

```
cd back-end
composer install
```

### Migrações de banco de dados

Depois de instalar as dependências do composer, adicione suas credenciais de banco de dados no arquivo `.env`, execute o camando no terminal. 
```
$ php artisan migrate

```
### Configurando Kernel

Em seguida, se você planeja utilizar o Sanctum para autenticar um SPA, deve adicionar o middleware do Sanctum ao seu apigrupo de middleware dentro do seu arquivo:app/Http/Kernel.php
````
'use Laravel\Sanctum\Http\Middleware\EnsureFrontendRequestsAreStateful;
 
 'api' => [
     EnsureFrontendRequestsAreStateful::class,
     'throttle:60,1',
     \Illuminate\Routing\Middleware\SubstituteBindings::class,
 ],
````


Agora, execute o run `artisan serve` command. Ele executará o aplicativo na URL `http://127.0.0.1:8000`.

```
$ php artisan serve
```

Se você está executando a API do Laravel em um caminho de URL diferente da `http://127.0.0.1:8000`, então você precisa atualizar o caminho do URL no `src/apis/Api.js` do Vue.js app.

# Vue.js Setup

```
cd front-end-vue
npm install
```

### Compilar e recarregar em desenvolvimento

```
npm run serve
```
### Compilar em produção
```
npm run build
```
### Verificar erros no código
```
npm run lint
```